import React from 'react';
import {mount} from '@cypress/react';
import App from '../../src/app';

describe('Renders app', () => {

    context('Render basic app', () => {
        it('renders app', () => {
            mount(<App />);
            cy.get('div').contains('my site').should('be.visible');
        });
    })
});
const { startDevServer } = require('@cypress/webpack-dev-server')
const webpackConfig = require('../../webpack.config.dev');

module.exports = (on, config) => {
    on('dev-server:start', async (options) =>
        startDevServer({ options, webpackConfig }),
    )
    return config
}

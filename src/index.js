import React from "react";
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux'
import App from './app';
import configureStore from "./redux/configure-store";
const store = configureStore();

const container = document.querySelector("#root");
const root = createRoot(container);
root.render(
    <Provider store={store}>
        <App />
    </Provider>,
    );

if (window.Cypress) {
    window.store = store
}
